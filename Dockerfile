FROM node:18-alpine as deps
COPY ./package.json ./yarn.lock .
RUN yarn --frozen-lockfile

FROM node:18-alpine as builder
WORKDIR /build
COPY ./prisma ./prisma
COPY ./package.json .
COPY --from=deps /node_modules node_modules
COPY . .
RUN yarn build

FROM node:18-alpine
COPY --from=deps /node_modules node_modules
COPY --from=builder /build/dist /dist
EXPOSE $PORT
ENTRYPOINT ["node", "dist/index.js"]